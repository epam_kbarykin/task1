import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class ReadFile {

    public ReadFile() {
    }

    public static int[] numbersFromText() throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader("Numbers.txt"));
        String str = reader.readLine();
        String formatted = str.replaceAll("\\D", " ");
        String[] strArr = formatted.split(" ");
        int[] numArr = new int[strArr.length];

        for (int i = 0; i < strArr.length; ++i) {
            numArr[i] = Integer.parseInt(strArr[i]);
        }

        return numArr;
    }
}
