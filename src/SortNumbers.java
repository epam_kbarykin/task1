import java.util.Arrays;

public class SortNumbers {
    public SortNumbers() {
    }

    public static void ascendingPrint(int[] arr) {
        Arrays.sort(arr);
        System.out.print("По возрастанию: ");

        for (int i = 0; i < arr.length; ++i) {
            System.out.print(arr[i] + " ");
        }

    }

    public static void descendingPrint(int[] arr) {
        int last = arr.length;
        System.out.print("\nПо убыванию: ");

        for (boolean sorted = last == 0; !sorted; --last) {
            sorted = true;

            for (int i = 1; i < last; ++i) {
                if (arr[i - 1] < arr[i]) {
                    sorted = false;
                    int tmp = arr[i - 1];
                    arr[i - 1] = arr[i];
                    arr[i] = tmp;
                }
            }
        }

        for (int i = 0; i < arr.length; ++i) {
            System.out.print(arr[i] + " ");
        }

    }
}
