import java.io.IOException;

public class MainClass {
    public static void main(String[] args) throws IOException {
        SortNumbers.ascendingPrint(ReadFile.numbersFromText());
        SortNumbers.descendingPrint(ReadFile.numbersFromText());
    }
}
